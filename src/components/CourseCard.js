import {Card, Button} from 'react-bootstrap'
const CourseCard = () => {
  return (

   <Card className="highlightCard p-3 mt-4">
  <Card.Body>
    <Card.Title>Sample Course</Card.Title>
    <Card.Subtitle className="mb-2">
     Description:
    </Card.Subtitle>
 
      <Card.Text>This is a sample course offering.</Card.Text>
      <Card.Text className="mb-0">Price:</Card.Text>
      <Card.Text>Php 40,000</Card.Text>

    <Button variant="primary">Enroll</Button>
  </Card.Body>
</Card>


    )
}
export default CourseCard;
